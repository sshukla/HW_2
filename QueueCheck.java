// Data Structures & Algorithms
// Spring 2018

// testing the correctness (not the performance) of a queue


import java.util.List;
import java.util.ArrayList;


public class QueueCheck {

    public static void main(String[] args)  {
        List<Integer> list = new ArrayList<Integer>();
        final int N = 100000;
        for (int i = 0; i < N; i++) {
            list.add(i * i);
        }
        circularTest(list);
    }

    public static <T> void circularTest(List<T> list) {
        QueueInterface<T> q = new ArrayListQueue<T>();
        QueueInterface<T> r = new ArrayListQueue<T>();
        // populate two queues with same sequence of items as in list
        for (T x : list) {
            q.enqueue(x);
            r.enqueue(x);
        }
        // rotate items in q one full rotation
        for (T x : list) {
            q.enqueue(q.dequeue());
        }
        for (T x : list) {
            T y = q.dequeue(); 
            if (!x.equals(y)) {
                throw new RuntimeException("list and queues do not match: " + x + " v. " + y);
            }
            if (!y.equals(r.dequeue())) {
                throw new RuntimeException("queues do not match on removal of: " + y);
            }
        }
        if (!q.isEmpty() || !r.isEmpty()) {
            throw new RuntimeException("both queues should now be empty");
        }
    }

}