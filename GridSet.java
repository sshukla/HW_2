
/**
 * Write a description of class GridSet here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class GridSet implements GridSetInterface
{
    private GridCounter _grid;


    public GridSet(GridCounter grid)
    {
        _grid = grid;
    }


    public boolean test(int x, int y)
    {
        if(x<0 || y<0 || x >= _grid.rows() || y >= _grid.columns())
            throw new IndexOutOfBoundsException("<" + x + ", " + y + ">");
        return _grid.get(x,y) > 0;
    }
    
    public void set(int x, int y)
    {
        if(x<0 || y<0 || x >= _grid.rows() || y >= _grid.columns())
            throw new IndexOutOfBoundsException("<" + x + ", " + y + ">");
        _grid.increment(x,y);
    }
}
