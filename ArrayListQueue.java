// Data Structures & Algorithms
// Spring 2018
// HW2: bfs/dfs traversal


import java.util.ArrayList;


/**
 * generic queues using java.util.ArrayList
 * 
 * @author Shail Shukla
 */
public class ArrayListQueue<T> implements QueueInterface<T> {

    private ArrayList<T> _array;
    private int _size;
    private int _front;
    private int _back;
    
    public ArrayListQueue() { 
        _array = new ArrayList<T>();
        _front = 0;
        _back = 0; 
        _size = 0;
    }

    public boolean isEmpty() {
        return  _size==0; 
    }

    
    public void enqueue(T element) {
        if(_front  == 0 && _back == 0){
            _array.add(element);
            _size++;
        }else if(_back == _array.size() && _array.size()%_size == 0){
            _array.add(element);
            _back++;
            _size++;
        }else if (_back != _front){
            _array.add(_back + (_array.size() % _size), element);
            _back++;
            _size++;
        }else{
            _array.add(_back,element);
            _back++;
            _front++;
            _size++;
        }

    }

    
    public T dequeue() {
        if (!isEmpty()) {
            T x = _array.get(_front);
            if(_front != _array.size()){
                _front++;
                _size--;
            } else{
                _front = 0; 
                _size--;
            }
            return x;
        } else {
            throw new RuntimeException("queue empty");
        }
    }
    
}
