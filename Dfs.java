// Data Structures & Algorithms
// Spring 2018
// HW2: bfs/dfs traversal


/**
 * depth-first grid traversal 
 * 
 * @author Shail Shukla
 */
public class Dfs implements TraverserInterface {

    private ArrayListStack _stack;
    private GridCounter _grid;
    private GridSet _gs;
    
    private void depthfs(Pair<Integer,Integer> start, GridSet gr){
        Pair _a;
        Pair _b;
        Pair _c;
        Pair _d;
        _stack.push(start);
        if(!gr.test(start.first()+1,start.second())){
            _a = new Pair(start.first()+1,start.second());
            _stack.push(_a);
            gr.set(start.first()+1,start.second());
        }
        if(!gr.test(start.first()-1,start.second())){
            _b = new Pair(start.first()-1,start.second());
            _stack.push(_b);
            gr.set(start.first()-1,start.second());
        } 
                    if(!gr.test(start.first(),start.second()+1)){
            _c = new Pair(start.first(),start.second()+1);
            _stack.push(_c);
            gr.set(start.first(),start.second()+1);
        }
                if(!gr.test(start.first(),start.second()-1)){
            _d = new Pair(start.first(),start.second()-1);
            _stack.push(_d);
            gr.set(start.first(),start.second()-1);
        }
        depthfs(_stack.pop(),gr);
    }
    
    public Dfs(int columns, int rows, Pair<Integer, Integer> origin) {
        _grid = new GridCounter(rows, columns);
        _gs = new GridSet(_grid);
        _gs.set(origin.first(),origin.second());
        
        depthfs(origin, _gs);
        
    }
    
    public boolean hasNext() {
        return _stack.isEmpty();
    }

    public Pair<Integer, Integer> next() {
        return _stack.pop();
    }

    // ...
}
