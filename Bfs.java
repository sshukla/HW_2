// Data Structures & Algorithms
// Spring 2018
// HW2: bfs/dfs traversal


/**
 * breadth-first grid traversal
 * 
 * @author Shail Shukla
 */
public class Bfs implements TraverserInterface {
    
    private ArrayListQueue _queue;
    private GridCounter _grid;
    private GridSet _gs;
    
    
        private void breathfs(Pair<Integer,Integer> start, GridSet gr){
        Pair _a;
        Pair _b;
        Pair _c;
        Pair _d;
        _queue.enqueue(start);
        if(!gr.test(start.first()+1,start.second())){
            _a = new Pair(start.first()+1,start.second());
            _queue.enqueue(_a);
            gr.set(start.first()+1,start.second());
        }
        if(!gr.test(start.first()-1,start.second())){
            _b = new Pair(start.first()-1,start.second());
            _queue.enqueue(_b);
            gr.set(start.first()-1,start.second());
        } 
                    if(!gr.test(start.first(),start.second()+1)){
            _c = new Pair(start.first(),start.second()+1);
            _queue.enqueue(_c);
            gr.set(start.first(),start.second()+1);
        }
                if(!gr.test(start.first(),start.second()-1)){
            _d = new Pair(start.first(),start.second()-1);
            _queue.enqueue(_d);
            gr.set(start.first(),start.second()-1);
        }
        breathfs(_queue.dequeue(),gr);
    }
    
    public Bfs(int columns, int rows, Pair<Integer, Integer> origin) {
        _grid = new GridCounter(rows, columns);
        _gs = new GridSet(_grid);
        _gs.set(origin.first(),origin.second());
        
        breathfs(origin, _gs);
    }
    
    public boolean hasNext() {
        return _queue.isEmpty();
    }

    public Pair<Integer, Integer> next() {
        return _queue.dequeue();
    }

    // ...
}
