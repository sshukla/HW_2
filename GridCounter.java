// Data Structures & Algorithms
// Spring 2018
// HW2: bfs/dfs traversal


/**
 * two-dimensional grid of counters 
 * 
 * @author Shail Shukla
 */
public class GridCounter implements GridCounterInterface {

    private int _columns;
    private int _rows;
    private int _grid[][];
    
    public GridCounter(int columns, int rows) {
        _grid = new int[rows][columns];
        for(int i = 0; i < rows; i++){
            for(int j = 0; j < columns; j++){
                _grid[i][j] = 0;
            }
        }
        _columns = columns;
        _rows = rows;
    }

    public int columns() {
        return _columns;
    }

    public int rows() {
        return _rows;
    }

    public int zeros() {
        int x = 0;
         for(int i = 0; i < _rows; i++){
            for(int j = 0; j < _columns; j++){
                if(_grid[i][j] == 0){
                    x++;
                }
            }
        }
        return x;
    }

    public int get(int x, int y) {
        if(x<0 || y<0 || x >= _rows || y >= +_columns)
            throw new IndexOutOfBoundsException("<" + x + ", " + y + ">");
        return _grid[x][y];
    }
        
    public void increment(int x, int y) {
        if(x<0 || y<0 || x >= _rows || y >= +_columns)
            throw new IndexOutOfBoundsException("<" + x + ", " + y + ">");
        _grid[x][y] += 1;
    }

}
