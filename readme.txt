Data Structures & Algorithms - Spring 2018
==========================================

## Programming Assignment 2
### breadth-first and depth-first traversals of two-dimensional grids
#### due date: Friday, February 23 at 5pm


-----------------------------------------------------------------

### Instructions

* Work on your own.
* Before writing any code, read through all the files in this
  archive.
* Unless otherwise indicated, use only the Java operations,
  commands, and standard library features  we have been using
  thus far in class and lab. (If you are unsure whether you can
  use something, just ask.) 
* Submit your work by creating a GitLab project, committing and pushing
  your work to that repository, sharing the repository with me with at
  least Developer permissions. (The act of sharing will alert me to the
  URL for the repository.)


-----------------------------------------------------------------

### The problems to completed

1. Complete the class ArrayListQueue so that is correctly and
   *efficiently* (in terms of both time and *memory*) implements generic
   queues using java's ArrayList class. The starter code is a version we
   developed in lab which has average-case O(1) enqueue and dequeue, but
   can be very wasteful in terms of memory. This would be fairly easy to
   implement using a combination of two ideas we have already
   considered: fixed-cap queues where we keep track of front and back
   indexes and let them "wrap around"; and explicitly resized
   arrays. However, because Java arrays and Java generics do not always
   "play nice", we need to leverage what has already been built for us
   (i.e., ArrayList). 
   

     ------------------------------------------------------------

2. Complete GridCounter so it correctly implements
   GridCounterInterface. The idea is to keep track of a two-dimensional
   "grid" of counters that all start at 0 and be incremented but not
   reset or decremented. I suggest using a two-dimensional array of
   integers as the underlying data structure.
   
3. Create public class GridSet that implements GridSetInterface. Do not
   reinvent the wheel: make your class a simple wrapper around a
   GridCounter.
   
4. Complete Pair<T, U> so that it correctly implements PairInterface<T,
   U> - namely it represents immutable generic pairs. (For our purposes
   here, immutable simple means that once created the underlying values
   cannot be changed through the methods of this class.)

      --------------------------------------------------

   At this point, GuiTraverse should run correctly when given no
   arguments - it visually demonstrate a random walk. TextTraverse will
   work on the random walk, but will incorrectly report results for Bfs
   and Dfs.


5. Complete Bfs so that it implements TraverserInterface and correctly
   represents the idea of a breadth-first traversal of a two-dimensional
   grid. I suggest using a queue and GridSet to avoid unnecessarily
   revisiting cells.

6. Complete Dfs so that it implements TraverserInterface and correctly
   represents the idea of a depth-first traversal of a two-dimensional
   grid. This should be *very similar* to Bfs, but should use a stack.


At this point both TextTraverse and GuiTraverse should work completely.


-----------------------------------------------------------------

### Challenge problems

* Write a class that thoroughly tests the behavior of stacks and queues
  both in terms of their correctness and their efficiency.

* Modify GuiTraverse so that the color of revisited nodes changes as a
  cell is visited more and more frequently.
   
* Add GUI features to GuiTraverse so the user can control which
  traversal methods are used without restarting the application.
   
* Add GUI features to GuiTraverse so the user can click the grid to
  start the traversal from the clicked location.
  
* Modify the comparison code (and your traversal classes if need be) to
  represent a distance traveled, not just the number of cells
  visited. (So, if a Dfs traversal jumps from (3, 4) to (17, 12) it
  should "cost" more than if the next cell visited is a neighbor.
